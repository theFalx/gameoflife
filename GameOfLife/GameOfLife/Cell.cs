﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Shapes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;



namespace GameOfLife
{
    class Cell
    {
        private readonly DockPanel _CellView;
        private readonly Ellipse _CellCircle;
        private Brush _CircleFill;
        private bool _IsAlive;

        #region Properties

        public DockPanel CellView
        {
            get{ return _CellView; }
        }

        public Brush CellFill
        {
            get { return _CircleFill; }
            set { _CellCircle.Fill = value; }
        }

        public bool IsAlive
        {
            set
            {
                
                if (_IsAlive != value)
                { // only do something if value is different from current _IsAlive!
                    Action action;
                    if (_IsAlive && !value)
                        action = new Action(delegate() { _CellView.Children.Remove(_CellCircle); });
                    else // if (!_IsAlive && value) -> not needed since it is already checked that _IsAlive != value !
                        action = new Action(delegate() { _CellView.Children.Add(_CellCircle); });

                    _IsAlive = value;

                    _CellView.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, action);
                }
            }

            get { return _IsAlive; }
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="circleFill"></param>
        /// <param name="radius"></param>
        public Cell(Brush circleFill, int width, int height)
        {
            int radius = Math.Min(width, height);
        
            this._CellView   = new DockPanel();
            this._CellCircle = new Ellipse();
            this._CircleFill = circleFill;

            this._CellView.Background = Brushes.White; //needed so the DockPanel (_CellView) does not resize!
            this._CellView.MinWidth   = width;
            this._CellView.MinHeight  = height;
            this._CellView.Width  = width;
            this._CellView.Height = height;
            this._CellView.MouseDown += new MouseButtonEventHandler(OnCellClick);

            this._CellCircle.Height = radius;
            this._CellCircle.Width  = radius;
            this._CellCircle.Fill   = circleFill;

            this._IsAlive  = false;
        }

        private void OnCellClick(object sender, RoutedEventArgs args)
        {
            IsAlive = !IsAlive;
        }

    }
}
