﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Diagnostics;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Xceed.Wpf.Toolkit;

namespace GameOfLife
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        private SimulatorCore simulatorCore;
        private Cell[,] cells;
        private bool controlEnabled = true;

        public MainWindow()
        {
            InitializeComponent();
            InitGameGrid(15, 15);
            btn_Stop.IsEnabled = false;
        }


        #region Private Methods

        private void InitGameGrid(int rows, int cols)
        {
            // clean the Grid if there are already Rows and Columns
            if (GameGrid.RowDefinitions.Count > 0)
                GameGrid.RowDefinitions.RemoveRange(0, GameGrid.RowDefinitions.Count);

            if (GameGrid.ColumnDefinitions.Count > 0)
                GameGrid.ColumnDefinitions.RemoveRange(0, GameGrid.ColumnDefinitions.Count);
            // -- 

            double width  = GameGrid.Width;
            double height = GameGrid.Height;

            int colSize = (int)width / cols;
            int rowSize = (int)height / rows;

            // ++ DEBUG ++ //
            System.Diagnostics.Debug.WriteLine(height);
            System.Diagnostics.Debug.WriteLine(rowSize);
            // -- DEBUG -- //

            for (int i = 0; i < rows; i++)
                GameGrid.RowDefinitions.Add(new RowDefinition());

            for (int i = 0; i < cols; i++)
                GameGrid.ColumnDefinitions.Add(new ColumnDefinition());
            
            cells = new Cell[rows,cols];

            for (int x = 0; x < rows; x++)
            {
                for (int y = 0; y < cols; y++)
                {
                    Cell c = new Cell(Brushes.Black, colSize, rowSize);
                    cells[x, y] = c;
                    Grid.SetColumn(c.CellView, y);
                    Grid.SetRow(c.CellView, x);
                    GameGrid.Children.Add(c.CellView);
                }
            }
        
            // ++ DEBUG ++ //
            Debug.WriteLine(cells.GetLength(0));
            Debug.WriteLine(cells.GetLength(1));
            // -- DEBUG -- //

            simulatorCore = new SimulatorCore { Cells = cells, Generations = SimulatorCore.INFINITE_GENERATIONS };

        }


        private void ToggleControlEnabled()
        {
            controlEnabled = !controlEnabled;

            // enable/disable settings controler
            sel_Columns.IsEnabled = controlEnabled;
            sel_Rows.IsEnabled    = controlEnabled;
            btn_Update.IsEnabled  = controlEnabled;
            slid_Speed.IsEnabled  = controlEnabled;
            bx_Speed.IsEnabled    = controlEnabled;

            // switch the state for Start-/Stopbuttons
            bool temp = btn_Stop.IsEnabled;
            btn_Stop.IsEnabled  = btn_Start.IsEnabled;
            btn_Start.IsEnabled = temp;

        }


        #endregion


        #region Click Handler

        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            ToggleControlEnabled();
            simulatorCore.Speed = Int32.Parse(bx_Speed.Text);
            Debug.WriteLine("Speed: {0} \tText: {1}", simulatorCore.Speed, Int32.Parse(bx_Speed.Text));
            simulatorCore.Start();
        }

        private void StopGame_Click(object sender, RoutedEventArgs e)
        {
            ToggleControlEnabled();
            simulatorCore.Stop();
        }

        private void UpdateField_Click(object sender, RoutedEventArgs e)
        {
            InitGameGrid(Int32.Parse(sel_Rows.Text), Int32.Parse(sel_Columns.Text));
        }

        #endregion
    }
}
