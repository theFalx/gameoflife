﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    /// <summary>
    /// +-----------> x
    /// |
    /// |
    /// |
    /// |
    /// y
    /// </summary>
    class SimulatorCore
    {

        public const int MAX_GENERATIONS = 200;
        public const int MIN_GENERATIONS = 1;
        public const int DEFAULT_GENERATIONS = 25;
        public const int INFINITE_GENERATIONS = Int32.MaxValue;


        // Speed: If there's a smaller 'timeout' between each evolution-step
        // then the simulation is faster
        public const int MAX_SPEED = 500;      // [ms]
        public const int MIN_SPEED = 5000;     // [ms]
        public const int DEFAULT_SPEED = 1000; //[ms]


        private volatile bool _IsRunning;
        private volatile bool stop;
        private Cell[,] _Cells;
        private int _Generations;
        private int _Speed;

        #region Properties

        public bool IsRunning
        {
            get { return _IsRunning; }
        }

        public Cell[,] Cells
        {
            set
            {
                if (_IsRunning)
                    throw new InvalidOperationException();
                _Cells = value;

                Debug.WriteLine(value.Length);
            }

            get { return _Cells; }
        }

        public int Generations
        {
            set 
            {
                if (value >= MIN_GENERATIONS && value <= MAX_GENERATIONS)
                    _Generations = value;
                else if (INFINITE_GENERATIONS == value)
                    _Generations = value;
            }

            get { return _Generations; }
        }

        public int Speed
        {
            set
            {
                if (value >= MAX_SPEED && value <= MIN_SPEED)
                    _Speed = value;
            }

            get { return _Speed; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public SimulatorCore()
        {
            this._Speed = DEFAULT_SPEED;
            this._Generations = DEFAULT_GENERATIONS;
        }

        #endregion


        #region Private Methods

        private bool alive(int neighbours)
        {
            return neighbours == 2 || neighbours == 3;
        }

        private bool revive(int neighbours)
        {
            return neighbours == 3;
        }

        private bool[,] prepareGeneration()
        {
            int ySize = _Cells.GetLength(0) + 2;
            int xSize = _Cells.GetLength(1) + 2;
            bool[,] generation = new bool[ySize, xSize];

            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    if (x == 0 || y == 0 || x == (xSize - 1) || y == (ySize - 1))
                        generation[y, x] = false;
                    else
                        generation[y, x] = _Cells[y - 1, x - 1].IsAlive;
                }
            }

            return generation;
        }

        private void switchGenerations(ref bool[,] genA, ref bool[,] genB)
        {
            bool[,] temp;
            temp = genA;
            genA = genB;
            genB = temp;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            if (_IsRunning)
                throw new InvalidOperationException();

            _IsRunning = true;
            stop = false;

            ThreadStart del = new ThreadStart(Run);
            (new Thread(del)).Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            if (_IsRunning)
            {
                stop = true;
                _IsRunning = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Run()
        {
            bool[,] activeGeneration  = prepareGeneration();
            bool[,] inactivGeneration = (bool[,])activeGeneration.Clone();
            int neighbours = 0;

            for (int g = 0;  g < Generations && !stop; g++)
            {
                for (int y = 1; y < (activeGeneration.GetLength(0) - 1); y++) {
                    for (int x = 1; x < (activeGeneration.GetLength(1) - 1); x++)
                    {
                        for (int i = (y - 1); i < (y + 2); i++) {
                            for (int j = (x - 1); j < (x + 2); j++)
                            {
                                if (activeGeneration[i, j] && !( y == i && x == j)) // only count if it's not the current field(x,y)
                                    neighbours++;
                            }
                        }


                        bool isAlive = activeGeneration[y, x] && alive(neighbours) || !activeGeneration[y, x] && revive(neighbours);
                        inactivGeneration[y, x] = isAlive;
                        _Cells[y - 1, x - 1].IsAlive = isAlive;

                        neighbours = 0;
                    }
                }

                switchGenerations(ref activeGeneration, ref inactivGeneration);

                Thread.Sleep(_Speed);
            }

            _IsRunning = false;
        }

        #endregion
    }
}
